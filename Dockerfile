FROM golang:alpine AS base
RUN mkdir /build
ADD go.mod go.sum main.go main_test.go /build/
WORKDIR /build
RUN apk update && \
    apk add libc-dev gcc git && \
    go mod tidy


FROM base AS compile
RUN CGO_ENABLED=0 \
    GOOS=linux \
    go build \
    -a -installsuffix cgo \
    -ldflags '-extldflags "-static"' \
    main.go


FROM alpine AS run
COPY --from=compile /build/main /bin/
CMD ["main"]


FROM base AS test
CMD ["go", "test", "-v"]
