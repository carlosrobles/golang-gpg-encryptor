down:
	docker-compose down

build: down
	docker-compose build golang-gpg-encryptor

test: test-build
	. ./env_vars; \
	docker-compose run test

test-build:
	docker-compose build test

up: build
	. ./env_vars; \
	docker-compose up -d

.PHONY: down build up test test-build
