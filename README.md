# golang-gpg-encryptor

A Go API that accepts a json input string and provides an ASCII armored GPG encrypted plaintext block. An instance of this program is available at https://golang-gpg-encryptor.robles.io and uses the public key contained in `env_vars`.

<br>

## What You'll Need

`docker`, `docker-compose`, and `make`. You will also need `jq` if you want to display the json output in a plaintext block with actual newlines instead of a json payload with encoded newlines (`\n`).

## Usage
Launch a local instance of golang-gpg-encryptor:

`make up`
<br>

Perform a GET request and receive an informational message:

```
curl --url http://127.0.0.1:8081/
{"message":"Please use a POST method with a string payload to receive an encrypted string"}
```
<br>

Perform a POST request to encrypt the string `secretpassword` and use `jq` to make it easy to copy and paste the encrypted output:

```
curl --url http://127.0.0.1:8081 --request POST --header "Content-Type: application/json"  --data '{"inputString": "secretpassword"}' | jq -r .encryptedString 
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   792  100   759  100    33   741k  33000 --:--:-- --:--:-- --:--:--  773k
-----BEGIN PGP MESSAGE-----

wcDMA0l3MqiUN+NWAQwAQ3ql+v+ek/TvMnIaUn5KqSzP/TaL5Z1I9coBTFQoRTPM
8wZFgFGFYEJCopFv7/a5okb+MuuH+D27lSdkv6H1SYrgzGEF8YDsSV9tSI2J4FEv
USi09bBAn/qF4aJQiVdKaV7BjIdPcc/DoxK3/pKPiT7Sgd+MeU3R+e246yJGAKb8
x0dDWSQ63YbbuoVcrEnlV0Nw/XfcYmGHa6QuwRn6qSbY0AHEvEEPGrAa38VBOsix
VY1cfXoeKF5utUpvHwQyEy8DIE1g1y2FlPNqtn+5uxJDe/7OcRD23F8FzbDJf343
IfGoxDsxNYNM5h8z/+HjinB73sv6sTqBlMG/aiM6JMowslUoota6K+glBaUcxLAF
QtFM1exlmLvd3d0QHAe8qJQFWGrPPggHWDGDrBin9k2j++ltDq/sjuPUnOw4IC+Z
22maiGn/rtdD3cnAGke6WOOPfUazn1Lwxlz0r8jheijMIQLJc7Yp3nvF9Ir8YgMm
8+PBBiXBxs2uV0/imxpr0uAB5C6PCHXAivC+3vfiVskmVbzhG1HgKuCW4cHr4DLi
JywpieA2464V97CqqnoS4LjitilCHOCj4UHm4J7kbjKzVTwBrEsnGdgDwkdJ1uI8
wEd84Xo0AA==
=EU8+
-----END PGP MESSAGE-----
```
<br>

Run the basic tests located in main_test.go:

```
make test

=== RUN   TestInfoMessage
--- PASS: TestInfoMessage (0.00s)
=== RUN   TestHandleEncryption
--- PASS: TestHandleEncryption (0.00s)
PASS
ok  	_/test	0.003s
```
<br>

Stop and destroy the container

`make down`
