module gitlab.com/carlosrobles/golang-gpg-encryptor

go 1.18

require (
	github.com/gorilla/mux v1.8.0
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e
)
