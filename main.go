package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	"golang.org/x/crypto/openpgp/packet"
	"log"
	"net/http"
	"os"
)

// error helper function
func check(err error, action string) {
	if err != nil {
		fmt.Println("error performing action: ", action)
		fmt.Println("error: ", err)
	}
}

type jsonScaffold struct {
	Message         string `json:"message,omitempty"`
	InputString     string `json:"inputString,omitempty"`
	EncryptedString string `json:"encryptedString,omitempty"`
}

// Encrypt an input string message with the desired pgp recipient
func encryptMessage(recipient *openpgp.Entity, message string) *bytes.Buffer {

	// Create new empty buffer
	encryptionBuffer := new(bytes.Buffer)

	// Create WriteCloser that will armor encode data written to it
	armorWriteCloser, err := armor.Encode(encryptionBuffer, "PGP MESSAGE", nil)
	check(err, "creating armor encoded WriteCloser")

	// Create WriteCloser that will encrypt data written to it
	gpgWriteCloser, err := openpgp.Encrypt(armorWriteCloser,
		[]*openpgp.Entity{recipient},
		nil,
		&openpgp.FileHints{IsBinary: true},
		nil)
	check(err, "creating gpg encryption WriteCloser")

	// Write to our encryption WriteCloser
	_, err = gpgWriteCloser.Write([]byte(message))
	check(err, "encrypting message")

	// Close WriteClosers
	err = gpgWriteCloser.Close()
	check(err, "closing gpg encryption WriteCloser")

	err = armorWriteCloser.Close()
	check(err, "closing gpg armored WriteCloser")

	// Return encrypted *bytes.Buffer
	return encryptionBuffer
}

// Take incoming http request and return the string value in the payload
func getJsonMessage(w http.ResponseWriter, r *http.Request) string {
	var jsonRequest jsonScaffold
	err := json.NewDecoder(r.Body).Decode(&jsonRequest)
	check(err, "reading JSON post")
	return jsonRequest.InputString
}

// Parse and encrypt input JSON payloads
// Return json output to user with encrypted string as value
func handleEncryption(w http.ResponseWriter, r *http.Request) {
	recipient := importPubKey(os.Getenv("GPG_PUBKEY"))
	message := getJsonMessage(w, r)
	if message != "" {
		encryptionBuffer := encryptMessage(recipient, message)
		jsonPaylod := returnEncryptedJson(encryptionBuffer)

		jsonResponse, err := json.Marshal(jsonPaylod)
		check(err, "marshalling json")

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(jsonResponse)
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}
}

// Input gpg armored public key and return a gpg entity
func importPubKey(publicKey string) *openpgp.Entity {
	// Read in public key
	r := bytes.NewBufferString(publicKey)

	// Create a gpg armored block
	gpgBlock, err := armor.Decode(r)
	check(err, "decoding public key")

	// Create gpg entity from public key block data
	recipient, err := openpgp.ReadEntity(packet.NewReader(gpgBlock.Body))
	check(err, "importing public key")

	return recipient
}

// Display helpful message on GET
func infoMessage(w http.ResponseWriter, r *http.Request) {
	response := jsonScaffold{Message: "Please use a POST method with a string payload to receive an encrypted string"}

	responseJson, err := json.Marshal(response)
	check(err, "marshalling JSON")

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(responseJson)
}

// Convert input *bytes.Buffer to a json struct
func returnEncryptedJson(e *bytes.Buffer) jsonScaffold {
	return jsonScaffold{EncryptedString: e.String()}
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", infoMessage).Methods("GET")
	router.HandleFunc("/golang-gpg-encryptor", infoMessage).Methods("GET")
	router.HandleFunc("/", handleEncryption).Methods("POST")
	router.HandleFunc("/golang-gpg-encryptor", handleEncryption).Methods("POST")
	log.Fatal(http.ListenAndServe(":8081", router))
}
