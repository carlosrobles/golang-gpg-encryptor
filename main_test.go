package main

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestInfoMessage(t *testing.T) {
	// Create request that will be passed off to handler
	request, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		fmt.Println("error generating request:", err)
	}

	// Record the response
	// NewRecorder() returns a ResponseRecorder struct
	// ResponseRecorder is an implementation of http.ResponseWriter
	response := httptest.NewRecorder()

	// Create handler to respond to an HTTP request
	// ServeHTTP writes reply headers and data to the ResponseWriter
	handler := http.HandlerFunc(infoMessage)
	handler.ServeHTTP(response, request)

	// Check the status code
	if status := response.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	//Check the response body
	expected := `{"message":"Please use a POST method with a string payload to receive an encrypted string"}`
	if response.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			response.Body.String(), expected)
	}
}

func TestHandleEncryption(t *testing.T) {
	// Create request that will be passed off to handler
	request, err := http.NewRequest("POST", "/", bytes.NewBufferString("{\"inputString\": \"supersecretshh\"}"))
	if err != nil {
		fmt.Println("error generating request:", err)
	}

	// Record the response
	// NewRecorder() returns a ResponseRecorder struct
	// ResponseRecorder is an implementation of http.ResponseWriter
	response := httptest.NewRecorder()

	// Create handler to respond to an HTTP request
	// ServeHTTP writes reply headers and data to the ResponseWriter
	handler := http.HandlerFunc(handleEncryption)
	handler.ServeHTTP(response, request)

	// Check the status code
	if status := response.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
}
